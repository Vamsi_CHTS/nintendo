package com.ch.report.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Logger;

import org.json.simple.JSONObject;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class ReportFileUtility
{

    private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private ReportFileUtility()
    {}

    public static String readContent(String file)
    {
        StringBuilder content = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(file)))
        {

            String line = reader.readLine();
            while (null != line)
            {
                content.append(line + "\n");
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e)
        {

            e.printStackTrace();
        }

        return content.toString();
    }

    public static void writeHTMLContent(String file, String content)
    {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file)))
        {

            writer.write(content);
            writer.close();
            LOGGER.info("Report generated");
            sendExecutionStatus();
            LOGGER.info("Sent execution code successfully...");
        } catch (IOException e)
        {

            e.printStackTrace();
        }
    }

    public static void sendExecutionStatus()
    {
        RestAssured.baseURI = "http://localhost:5000/queue/api";
        RequestSpecification request = RestAssured.given().contentType("application/json");

        JSONObject requestParams = new JSONObject();
        requestParams.put("emailsent", 0);
        requestParams.put("executioncomplete", 1);

        request.body(requestParams.toJSONString());
        Response response = request.post("/status");

        int statusCode = response.getStatusCode();
        LOGGER.info("The status code recieved: " + statusCode);
        LOGGER.info(response.body().asString());
    }

}
