package com.ch.report.utils;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.bson.Document;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.ch.email.EmailUtility;
import com.ch.nintendo.utils.constants.FileConstants;
import com.ch.nintendo.utils.constants.ObjectConstants;
import com.ch.reports.FileUtility;
import com.ch.reports.TestCaseDetail;
import com.ch.reports.TestCaseFactory;
import com.ch.reports.TestSummaryResult;
import com.ch.utils.DriverFactory;
import com.ch.utils.PropertyUtil;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ServerAddress;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class AbstractTestCaseReport extends CommonReportUtility
{

    private final static Logger       LOGGER             = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    static String                     currentUrl         = PropertyUtil.getPropertyValue(ObjectConstants.URL,
            FileConstants.SERVER_PROPERTY_FILE);
    static long                       start              = 0;
    static long                       end                = 0;

    static String                     startDateTime      = "";
    static String                     endDateTime        = "";

    static String                     s                  = "";

    static int                        i                  = 0;

    private static MongoClientOptions options            = MongoClientOptions.builder().connectionsPerHost(20)
            .connectTimeout(60 * 1000).socketKeepAlive(true).build();

    public static MongoClient         mongoClient        = new MongoClient(
            new ServerAddress(PropertyUtil.getConfigValue(FileConstants.MONGO_DB_IP),
                    Integer.valueOf(PropertyUtil.getConfigValue(FileConstants.MONGO_DB_PORT))),
            options);
    // mongoClient = new MongoClient( "137.117.88.119" , 27017 );

    int                               jsonid             = 0;

    public static String              TestSuiteName      = "";
    public static String              environmentName    = "";

    public static String              OS                 = "";
    public static String              date               = "";
    public static String              time               = "";
    public static String              totalExecutionTime = "";
    public static String              executionId        = "";
    // public AbstractTestCaseReport() {
    // }

    @BeforeSuite(alwaysRun = true)
    public static void timer()
    {
        start = System.currentTimeMillis();
        i = 0;
        if (PropertyUtil.getServerValue("url").contains("www."))
        {
            environmentName = "PRODUCTION";
        } else if (PropertyUtil.getServerValue("url").contains("dev-preview"))
        {
            environmentName = "DEV-PREVIEW";
        } else
        {
            environmentName = "STAGE";
        }
        if (PropertyUtil.getConfigValue("BROWSER_NAME").equalsIgnoreCase("iossafari"))
        {
            OS = "iOS";
        } else
        {
            OS = System.getProperty("os.name").toUpperCase();
        }
        date = CommonReportUtility.getDate();
        time = CommonReportUtility.getTime();
        executionId = PropertyUtil.getConfigValue("EXECUTION_ID");
    }

    @AfterSuite(alwaysRun = true)
    public static String endtime()
    {
        end = System.currentTimeMillis();
        long total = end - start;
        String format = String.format("%%0%dd", 2);
        total = total / 1000;
        String seconds = String.format(format, total % 60);
        String minutes = String.format(format, (total % 3600) / 60);
        String hours = String.format(format, total / 3600);
        String time = hours + ":" + minutes + ":" + seconds;
        return time;
    }

    public static void instantance()
    {
        DriverFactory.getDriver();
    }

    @AfterMethod
    public void testQuitDriver()
    {
        LOGGER.info("Browser CLosed");
    }

    public static String getdateAndTime()
    {
        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("MMM dd, yyyy 'at' hh:mm:ss a zzz");
        return ft.format(dNow);
    }

    // public static String getDate() {
    // Date dNow = new Date();
    // SimpleDateFormat ft = new SimpleDateFormat("MMM dd, yyyy");
    // return ft.format(dNow);
    // }

    @AfterSuite(alwaysRun = true)
    public void reportgenerator(ITestContext ctx)
    {
        String suiteName = ctx.getCurrentXmlTest().getSuite().getName();
        generateHTML(TestCaseFactory.getTestcases(), suiteName);
        LOGGER.info("Report generated");
        LOGGER.info("Done");
    }

    @BeforeMethod(alwaysRun = true)
    public static void suiteName(ITestContext ctx)
    {
        TestSuiteName = ctx.getCurrentXmlTest().getSuite().getName();
    }

    public static void sendPassOrFailInfo(TestCaseDetail testcase)
    {
        JSONObject TestCaseDetails = new JSONObject();
        TestCaseDetails.put("smoke", testcase.getSmokeName());
        TestCaseDetails.put("priority", testcase.getPriority());
        TestCaseDetails.put("moduleName", testcase.getModuleName());
        TestCaseDetails.put(FileConstants.SITENAME_CONSTRANT, PropertyUtil.getServerValue("siteName"));
        TestCaseDetails.put(FileConstants.TSUITNAME_CONSTRANT, TestSuiteName);
        TestCaseDetails.put(FileConstants.TCNAME_CONSTRANT, testcase.getTestcasename());
        TestCaseDetails.put(FileConstants.RELEASEVERSION_CONSTRANT,
                PropertyUtil.getConfigValue(FileConstants.RELEASE_VERSION));
        if (testcase.getExactResult().equalsIgnoreCase("PASS"))
        {
            TestCaseDetails.put(FileConstants.RESULT_CONSTRANT, " " + testcase.getExactResult());
        } else
        {
            TestCaseDetails.put(FileConstants.RESULT_CONSTRANT, testcase.getExactResult());
        }
        TestCaseDetails.put("date", date);
        TestCaseDetails.put("time", time);
        TestCaseDetails.put(FileConstants.ATTACHMENT_CONSTRANT, testcase.getImageString());
        if (testcase.getExactResult().equalsIgnoreCase("FAIL"))
        {
            TestCaseDetails.put(FileConstants.STACKTRACE_CONSTRANT, testcase.getErrorlog());
        } else
        {
            TestCaseDetails.put(FileConstants.STACKTRACE_CONSTRANT, testcase.getExpectedresult());
        }
        // TestCaseDetails.put("suitid", 3);
        TestCaseDetails.put(FileConstants.EXECUTIONID_CONSTRANT, executionId);
        TestCaseDetails.put(FileConstants.TCLASSNAME_CONSTRANT, testcase.getTestsuitename());
        TestCaseDetails.put("browserName", PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME));
        if (PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME).equalsIgnoreCase(FileConstants.BROWSER_CHROME))
        {
            TestCaseDetails.put(FileConstants.BROWSER_VERSION,
                    PropertyUtil.getConfigValue(FileConstants.CHROME_VERSION));
        } else if (PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME)
                .equalsIgnoreCase(FileConstants.BROWSER_FIREFOX))
        {
            TestCaseDetails.put(FileConstants.BROWSER_VERSION,
                    PropertyUtil.getConfigValue(FileConstants.FIREFOX_VERSION));
        } else if (PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME)
                .equalsIgnoreCase(FileConstants.BROWSER_SAFARI))
        {
            TestCaseDetails.put(FileConstants.BROWSER_VERSION,
                    PropertyUtil.getConfigValue(FileConstants.SAFARI_VERSION));
        } else if (PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME).equalsIgnoreCase(FileConstants.BROWSER_IE))
        {
            TestCaseDetails.put(FileConstants.BROWSER_VERSION, PropertyUtil.getConfigValue(FileConstants.IE_VERSION));
        }
        TestCaseDetails.put("environment", environmentName);
        TestCaseDetails.put("os", OS);
        if (testcase.getExactResult().trim().length() > 1)
        {
            Document doc = Document.parse(TestCaseDetails.toJSONString());
            // if (testcase.getTestsuitename().contains("Mobile")
            // ||
            // PropertyUtil.getConfigValue("MOBILE_PC").equalsIgnoreCase("mobile")
            // ||
            // PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME).contains("Mobile"))
            // {
            // mongoClient.getDatabase(PropertyUtil.getConfigValue(FileConstants.MONGO_DBNAME))
            // .getCollection(PropertyUtil.getConfigValue(FileConstants.MONGO_MOBILE_COLLECTION))
            // .insertOne(doc);
            // } else if (testcase.getTestsuitename().contains("Tablet")
            // ||
            // PropertyUtil.getConfigValue("MOBILE_PC").equalsIgnoreCase("Tablet")
            // ||
            // PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME).contains("Tablet"))
            // {
            // mongoClient.getDatabase(PropertyUtil.getConfigValue(FileConstants.MONGO_DBNAME))
            // .getCollection(PropertyUtil.getConfigValue(FileConstants.MONGO_TABLET_COLLECTION))
            // .insertOne(doc);
            // } else
            // {
            mongoClient.getDatabase(PropertyUtil.getConfigValue(FileConstants.MONGO_DBNAME))
                    .getCollection(PropertyUtil.getConfigValue(FileConstants.MONGO_DESKTOP_COLLECTION)).insertOne(doc);
            // }
        }
    }

    @AfterSuite(alwaysRun = true)
    public void writeResultsToJson(ITestContext ctx)
    {
        String suiteName = ctx.getCurrentXmlTest().getSuite().getName();
        ArrayList<TestCaseDetail> testcases = TestCaseFactory.getTestcases();
        List<Document> testDetailsPassList = new ArrayList<Document>();
        List<Document> testDetailsFailList = new ArrayList<Document>();
        List<Document> testDetailsErrorList = new ArrayList<Document>();
        JSONArray testDetailsArray = new JSONArray();
        for (Iterator<TestCaseDetail> iterator1 = testcases.iterator(); iterator1.hasNext();)
        {
            jsonid++;
            TestCaseDetail testcase = (TestCaseDetail) iterator1.next();
            JSONObject TestCaseDetails = new JSONObject();
            JSONObject ErrorTestCaseDetails = new JSONObject();
            TestCaseDetails.put("_id", jsonid);
            TestCaseDetails.put("smoke", testcase.getSmokeName());
            TestCaseDetails.put("priority", testcase.getPriority());
            TestCaseDetails.put("moduleName", testcase.getModuleName());
            TestCaseDetails.put(FileConstants.SITENAME_CONSTRANT, PropertyUtil.getServerValue("siteName"));
            TestCaseDetails.put(FileConstants.TSUITNAME_CONSTRANT, suiteName);
            TestCaseDetails.put(FileConstants.TCNAME_CONSTRANT, testcase.getTestcasename());
            TestCaseDetails.put(FileConstants.RELEASEVERSION_CONSTRANT,
                    PropertyUtil.getConfigValue(FileConstants.RELEASE_VERSION));
            TestCaseDetails.put(FileConstants.RESULT_CONSTRANT, testcase.getExactResult());
            TestCaseDetails.put("date", date);
            TestCaseDetails.put("time", time);
            TestCaseDetails.put(FileConstants.ATTACHMENT_CONSTRANT, testcase.getImageString());
            if (testcase.getExactResult().equalsIgnoreCase(FileConstants.ERROR_CONSTRANT))
            {
                TestCaseDetails.put(FileConstants.STACKTRACE_CONSTRANT, testcase.getTrace());
            } else if (testcase.getExactResult().equalsIgnoreCase("FAIL"))
            {
                TestCaseDetails.put(FileConstants.STACKTRACE_CONSTRANT, testcase.getErrorlog());
            } else
            {
                TestCaseDetails.put(FileConstants.STACKTRACE_CONSTRANT, testcase.getExpectedresult());
            }
            // TestCaseDetails.put("suitid", 3);
            TestCaseDetails.put(FileConstants.EXECUTIONID_CONSTRANT, executionId);
            TestCaseDetails.put(FileConstants.TCLASSNAME_CONSTRANT, testcase.getTestsuitename());
            TestCaseDetails.put("browserName", PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME));
            if (PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME).equalsIgnoreCase(FileConstants.BROWSER_CHROME))
            {
                TestCaseDetails.put(FileConstants.BROWSER_VERSION,
                        PropertyUtil.getConfigValue(FileConstants.CHROME_VERSION));
            } else if (PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME)
                    .equalsIgnoreCase(FileConstants.BROWSER_FIREFOX))
            {
                TestCaseDetails.put(FileConstants.BROWSER_VERSION,
                        PropertyUtil.getConfigValue(FileConstants.FIREFOX_VERSION));
            } else if (PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME)
                    .equalsIgnoreCase(FileConstants.BROWSER_SAFARI))
            {
                TestCaseDetails.put(FileConstants.BROWSER_VERSION,
                        PropertyUtil.getConfigValue(FileConstants.SAFARI_VERSION));
            } else if (PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME)
                    .equalsIgnoreCase(FileConstants.BROWSER_IE))
            {
                TestCaseDetails.put(FileConstants.BROWSER_VERSION,
                        PropertyUtil.getConfigValue(FileConstants.IE_VERSION));
            }
            TestCaseDetails.put("environment", environmentName);
            TestCaseDetails.put("os", OS);
            testDetailsArray.add(TestCaseDetails);
            if (testcase.getExactResult().equalsIgnoreCase("PASS"))
            {
                Document passDoc = Document.parse(TestCaseDetails.toJSONString());
                testDetailsPassList.add(passDoc);
            } else
            {
                Document failDoc = Document.parse(TestCaseDetails.toJSONString());
                testDetailsFailList.add(failDoc);
            }
            // if (testcase.getExactResult().equalsIgnoreCase("ERROR") &&
            // (testcase.getErrorlog() == ""
            // || testcase.getErrorlog() == null ||
            // testcase.getErrorlog().length() == 0))
            if (testcase.getExactResult().equalsIgnoreCase(FileConstants.ERROR_CONSTRANT)
                    && !(testcase.getErrorlog().trim().length() > 1))
            {
                ErrorTestCaseDetails.put("smoke", testcase.getSmokeName());
                ErrorTestCaseDetails.put("priority", testcase.getPriority());
                ErrorTestCaseDetails.put("moduleName", testcase.getModuleName());
                ErrorTestCaseDetails.put(FileConstants.SITENAME_CONSTRANT, PropertyUtil.getServerValue("siteName"));
                ErrorTestCaseDetails.put(FileConstants.TSUITNAME_CONSTRANT, suiteName);
                ErrorTestCaseDetails.put(FileConstants.TCNAME_CONSTRANT, testcase.getTestcasename());
                ErrorTestCaseDetails.put(FileConstants.RELEASEVERSION_CONSTRANT,
                        PropertyUtil.getConfigValue(FileConstants.RELEASE_VERSION));
                ErrorTestCaseDetails.put(FileConstants.RESULT_CONSTRANT, testcase.getExactResult());
                ErrorTestCaseDetails.put("date", date);
                ErrorTestCaseDetails.put("time", time);
                ErrorTestCaseDetails.put(FileConstants.ATTACHMENT_CONSTRANT, testcase.getImageString());
                if (testcase.getExactResult().equalsIgnoreCase("ERROR"))
                {
                    ErrorTestCaseDetails.put(FileConstants.STACKTRACE_CONSTRANT, testcase.getTrace());
                } else if (testcase.getExactResult().equalsIgnoreCase("FAIL"))
                {
                    ErrorTestCaseDetails.put(FileConstants.STACKTRACE_CONSTRANT, testcase.getErrorlog());
                } else
                {
                    ErrorTestCaseDetails.put(FileConstants.STACKTRACE_CONSTRANT, testcase.getExpectedresult());
                }
                // TestCaseDetails.put("suitid", 3);
                ErrorTestCaseDetails.put(FileConstants.EXECUTIONID_CONSTRANT, executionId);
                ErrorTestCaseDetails.put(FileConstants.TCLASSNAME_CONSTRANT, testcase.getTestsuitename());
                ErrorTestCaseDetails.put(FileConstants.BROWSER_VERSION,
                        PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME));
                if (PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME)
                        .equalsIgnoreCase(FileConstants.BROWSER_CHROME))
                {
                    ErrorTestCaseDetails.put(FileConstants.BROWSER_VERSION,
                            PropertyUtil.getConfigValue(FileConstants.CHROME_VERSION));
                } else if (PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME)
                        .equalsIgnoreCase(FileConstants.BROWSER_FIREFOX))
                {
                    ErrorTestCaseDetails.put(FileConstants.BROWSER_VERSION,
                            PropertyUtil.getConfigValue(FileConstants.FIREFOX_VERSION));
                } else if (PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME)
                        .equalsIgnoreCase(FileConstants.BROWSER_SAFARI))
                {
                    ErrorTestCaseDetails.put(FileConstants.BROWSER_VERSION,
                            PropertyUtil.getConfigValue(FileConstants.SAFARI_VERSION));
                } else if (PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME)
                        .equalsIgnoreCase(FileConstants.BROWSER_IE))
                {
                    ErrorTestCaseDetails.put(FileConstants.BROWSER_VERSION,
                            PropertyUtil.getConfigValue(FileConstants.IE_VERSION));
                }
                ErrorTestCaseDetails.put("environment", environmentName);
                ErrorTestCaseDetails.put("os", OS);
                Document errorDoc = Document.parse(ErrorTestCaseDetails.toJSONString());
                testDetailsErrorList.add(errorDoc);
            }
        }
        try (FileWriter file = new FileWriter(
                System.getProperty("test.home", "./" + "JsonFiles/") + suiteName + getCurrentDate() + ".json"))
        {
            file.write(testDetailsArray.toJSONString());
            file.flush();
            file.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        // if (testDetailsPassList.size() > 0)
        // {
        // mongoClient.getDatabase("TADATABASE").getCollection("TA").insertMany(testDetailsPassList);
        // }
        // if (testDetailsFailList.size() > 0)
        // {
        // mongoClient.getDatabase("TADATABASE").getCollection("TA_ERROR").insertMany(testDetailsFailList);
        // }
        if (testDetailsErrorList.size() > 0)
        {
            mongoClient.getDatabase(PropertyUtil.getConfigValue(FileConstants.MONGO_DBNAME))
                    .getCollection(PropertyUtil.getConfigValue(FileConstants.MONGO_CRASH_COLLECTION))
                    .insertMany(testDetailsErrorList);
        }
        // mongoClient.close();
    }

    public static void sendReportName(String name, String exId)
    {
        RestAssured.baseURI = "http://localhost:5000/queue/api";
        RequestSpecification request = RestAssured.given().contentType("application/json");

        JSONObject requestParams = new JSONObject();
        requestParams.put("reportname", name);
        requestParams.put("executionId", exId);
        requestParams.put("browserName", PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME));

        request.body(requestParams.toJSONString());
        Response response = request.post("/reportname");

        int statusCode = response.getStatusCode();
        LOGGER.info("The status code recieved: " + statusCode);
        LOGGER.info(response.body().asString());
    }

    public static void sendResultStatus(String pass, String fail, String error, String time, int testcaseCount)
    {
        RestAssured.baseURI = "http://localhost:5000/queue/api";
        RequestSpecification request = RestAssured.given().contentType("application/json");

        JSONObject requestParams = new JSONObject();
        requestParams.put("passpercentage", pass);
        requestParams.put("failpercentage", fail);
        requestParams.put("errorpercentage", error);
        requestParams.put("executiontime", time);
        requestParams.put("testcasecount", testcaseCount);

        request.body(requestParams.toJSONString());
        Response response = request.post("/resultstatus");

        int statusCode = response.getStatusCode();
        LOGGER.info("The status code recieved: " + statusCode);
        LOGGER.info(response.body().asString());
    }

    public void generateHTML(ArrayList<TestCaseDetail> list, String name)
    {
        try
        {
            String projecttitle = PropertyUtil.getConfigValue(FileConstants.PROJECT_TITLE);
            String starthtml = "<html>";
            String head = FileUtility.readContent(ReportFileConstants.HEAD_FILE);
            String body = "<body>";
            String endhtml = "</body></html>";
            ArrayList<TestSummaryResult> testSuites = getAllTestSuites(list);
            String summaryTable = generateSummaryTable(testSuites);
            String detailReport = generateCaseDetailTable(testSuites, list);
            /*
             * String htmlContent = starthtml + head + body + "<center><h3>" +
             * projecttitle + "</h3></center>" +
             * "<center><u><h4>Total Execution Time: " + endtime() +
             * "</h4></u></center>" + summaryTable + detailReport + endhtml;
             */
            String htmlContent = starthtml + head + body + "<center><h3>" + projecttitle + "</h3></center>"
                    + "<center><h3>" + "URL: " + PropertyUtil.getServerValue("url") + "</h3></center>" + "<center><h3>"
                    + "Browser: " + PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME) + "</h3></center>"
                    + "<center><u><h4>Total Execution Time: " + totalExecutionTime + "</h4></u></center>" + summaryTable
                    + detailReport + endhtml;
            String currentDate = getCurrentDate();
            String string = PropertyUtil.getServerValue("environment") + "_" + name + "_" + currentDate + ".html";
            sendReportName(string, executionId);
            String file = "";
            if (PropertyUtil.getConfigValue("LOGFILE_CONTROL").equalsIgnoreCase("Server"))
            {
                file = PropertyUtil.getConfigValue(FileConstants.LOG_PATH) + string;
                ReportFileUtility.writeHTMLContent(file, htmlContent);
                EmailUtility.sendFile(name + " Test CaseReport at - " + currentDate, htmlContent, file, string);
            } else if (PropertyUtil.getConfigValue("LOGFILE_CONTROL").equalsIgnoreCase("Local"))
            {
                file = ReportFileConstants.REPORTS_HOME + string;
                ReportFileUtility.writeHTMLContent(file, htmlContent);
                EmailUtility.sendFile(name + " Test CaseReport at - " + currentDate, htmlContent, file, string);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static String generateSummaryTable(ArrayList<TestSummaryResult> testsuites)
    {
        if (0 == testsuites.size())
        {
            return "";
        }
        String starttable = FileUtility.readContent(ReportFileConstants.TABLEHEAD_FILE);
        String content = "";
        String Total = "Total";
        int passcount = 0;
        int failedcount = 0;
        int errorcount = 0;
        int getTotalCounter = 0;
        long percentage = 0;
        long passpercentage = 0;
        long failpercentage = 0;
        long errorpercentage = 0;
        for (Iterator<TestSummaryResult> iterator = testsuites.iterator(); iterator.hasNext();)
        {
            TestSummaryResult testSuite = (TestSummaryResult) iterator.next();
            String color = "red";

            if ("-".equalsIgnoreCase(testSuite.getFailCount()))
            {
                color = "green";
            }
            if ("-".equalsIgnoreCase(testSuite.getErrorCount()))
            {
                color = "green";
            }

            getTotalCounter = getTotalCounter + testSuite.getTotal();

            // passed
            if (testSuite.getPassCount().equals("-"))
            {
                passcount = passcount + Integer.parseInt(testSuite.getPassCount().replace("-", "0"));
            } else
            {
                passcount = passcount + Integer.parseInt(testSuite.getPassCount());
            }

            // failed
            if (testSuite.getFailCount().equals("-"))
            {
                failedcount = failedcount + Integer.parseInt(testSuite.getFailCount().replace("-", "0"));
            } else
            {
                failedcount = failedcount + Integer.parseInt(testSuite.getFailCount());
            }
            // error
            if (testSuite.getErrorCount() == "-")
            {
                errorcount = errorcount + Integer.parseInt(testSuite.getErrorCount().replace("-", "0"));
            } else
            {
                errorcount = errorcount + Integer.parseInt(testSuite.getErrorCount());
            }

            content = content + FileConstants.TRTD_CONTANT + testSuite.getSuiteName() + FileConstants.TDTD_CONSTANT
                    + testSuite.getTotal() + FileConstants.TDTD_CONSTANT + testSuite.getPassCount()
                    + "</td><td><font color= \"" + color + "\">" + testSuite.getFailCount() + "</font color></td><td>"
                    + testSuite.getErrorCount() + "</td><td>" + testSuite.getPercentage() + "%</td></tr>";

            // content = content + "<tr><td>" + testSuite.getSuiteName() +
            // "</td><td>" + testSuite.getTotal()
            // + "</td><td><font color=\"green\">" + testSuite.getPassCount() +
            // "</td><td><font color=\"Blue\">"
            // + testSuite.getFailCount() + "</td><td><font color=\"red\">" +
            // testSuite.getErrorCount()
            // + "</td><td>" + testSuite.getPercentage() + "%</td></tr>";

        }
        percentage = Math.round((passcount / (getTotalCounter + 0.0)) * 100.0);

        passpercentage = Math.round((passcount / (getTotalCounter + 0.0)) * 100.0);
        failpercentage = Math.round((failedcount / (getTotalCounter + 0.0)) * 100.0);
        errorpercentage = Math.round((errorcount / (getTotalCounter + 0.0)) * 100.0);
        if ((passpercentage + failpercentage + errorpercentage) == 101)
        {
            errorpercentage = errorpercentage - 1;
        }

        content = content + FileConstants.TRTD_CONTANT + Total + FileConstants.TD_CONTANT;
        content = content + FileConstants.TD_CONTANT1 + getTotalCounter + FileConstants.TD_CONTANT;
        content = content + FileConstants.TD_CONTANT1 + passcount + FileConstants.TD_CONTANT;
        content = content + FileConstants.TD_CONTANT1 + failedcount + FileConstants.TD_CONTANT;
        content = content + FileConstants.TD_CONTANT1 + errorcount + FileConstants.TD_CONTANT;
        content = content + FileConstants.TD_CONTANT1 + percentage + "%</td></tr>";

        /*
         * content = content + "<tr><td>" + Total + "</td>"; content = content +
         * "<td>" + getTotalCounter + "</td>"; content = content + "<td>" +
         * passcount + "</td>"; content = content + "<td>" + failedcount +
         * "</td>"; content = content + "<td>" + errorcount + "</td>"; content =
         * content + "<td>" + percentage + "%</td></tr>";
         */

        String endtable = "</table>";
        totalExecutionTime = endtime();
        sendResultStatus(passpercentage + "%", failpercentage + "%", errorpercentage + "%", totalExecutionTime,
                getTotalCounter);
        return starttable + "" + content + "" + endtable;

    }

    public static String buildDetails()
    {
        String build = "<table class=\"blue\" align=\"center\"><tr><th align=\"center\" height=\"25\""
                + " bgcolor=\"#2E86C1\" colSpan=\"2\"><font color = \"#FFFFFF\" >Execution Details</font "
                + "color></th></tr><tr><td>URL</a></td><td>"
                + PropertyUtil.getPropertyValue("BOTURL", FileConstants.SERVER_PROPERTY_FILE)
                + "<tr><td>Browser</td><td>"
                + PropertyUtil.getPropertyValue("BROWSER_NAME", FileConstants.SERVER_PROPERTY_FILE)
                + FileConstants.TDTR_CONSTANT + "</td></tr><tr><td>Execution Time</td><td>" + startDateTime
                + FileConstants.TDTR_CONSTANT + "<tr><td>Duration</a></td><td>" + endtime() + "</td></tr></table>";
        return build;
    }

    public static String generateCaseDetailTable(ArrayList<TestSummaryResult> testSuites,
            ArrayList<TestCaseDetail> testcases)
    {
        if (0 == testcases.size())
        {
            return "";
        }
        String starttable = FileUtility.readContent(ReportFileConstants.TABLE_DEATIL_CASE_HEAD_FILE);
        String content = "";

        for (Iterator<TestSummaryResult> iterator = testSuites.iterator(); iterator.hasNext();)
        {
            TestSummaryResult suite = (TestSummaryResult) iterator.next();
            boolean flag = true;
            for (Iterator<TestCaseDetail> iterator1 = testcases.iterator(); iterator1.hasNext();)
            {
                TestCaseDetail testcase = (TestCaseDetail) iterator1.next();
                if (suite.getSuiteName().equalsIgnoreCase(testcase.getTestsuitename()))
                {
                    String color;
                    /*
                     * if ("Pass".equalsIgnoreCase(testcase.getResult())) {
                     * color = "green"; } else { color = "red"; }
                     */
                    if ("Pass".equalsIgnoreCase(testcase.getResult()))
                    {
                        color = "green";
                    } else if ("FAIL".equalsIgnoreCase(testcase.getResult()))
                    {
                        color = "Blue";
                    } else
                    {
                        color = "red";
                    }
                    if (true == flag)
                    {
                        content = content + "<tr><td rowspan=" + suite.getTotal() + ">" + suite.getSuiteName()
                                + FileConstants.TDTD_CONSTANT + testcase.getTestcasename() + FileConstants.TDTD_CONSTANT
                                + testcase.getExpectedresult() + "</td><td> <font color= \"" + color + "\">"
                                + testcase.getResult() + FileConstants.TDTD_CONSTANT + testcase.getErrorlog() + "</td>"
                                + "<td>" + testcase.getComment() + "</td></tr>";
                        flag = false;
                    } else
                    {
                        content = content + "<tr><td>" + testcase.getTestcasename() + "</td><td>"
                                + testcase.getExpectedresult() + "</td><td> <font color= \"" + color + "\">"
                                + testcase.getResult() + "</td><td>" + testcase.getErrorlog() + "</td>" + "<td>"
                                + testcase.getComment() + "</td></tr>";
                    }
                }
            }
        }
        String endtable = "</table>";
        return starttable + "" + content + "" + endtable;
    }

    private synchronized ArrayList<TestSummaryResult> getAllTestSuites(ArrayList<TestCaseDetail> list)
    {
        HashMap<String, TestSummaryResult> suites = new HashMap<String, TestSummaryResult>();
        for (Iterator<TestCaseDetail> iterator = list.iterator(); iterator.hasNext();)
        {
            TestCaseDetail testCaseDetail = (TestCaseDetail) iterator.next();
            String suite = testCaseDetail.getTestsuitename();
            int pass = 0;
            int fail = 0;
            int error = 0;
            if (testCaseDetail.getResult().equalsIgnoreCase("PASS"))
            {
                pass = 1;
            } else if (testCaseDetail.getResult().equalsIgnoreCase("ERROR"))
            {
                error = 1;
            } else
            {
                fail = 1;
            }
            if (suites.get(suite) == null)
            {
                suites.put(suite, new TestSummaryResult(suite, pass, fail, error));
            } else
            {
                TestSummaryResult result = suites.get(suite);
                if (pass == 1)
                {
                    result.incrementPass();
                } else if (error == 1)
                {
                    result.incrementError();
                } else
                {
                    result.incrementFail();
                }
            }
        }
        Set<String> set = suites.keySet();
        ArrayList<TestSummaryResult> testsuites = new ArrayList<TestSummaryResult>();
        for (Iterator<String> iterator = set.iterator(); iterator.hasNext();)
        {
            String name = (String) iterator.next();
            testsuites.add(suites.get(name));
        }
        return testsuites;
    }

}
