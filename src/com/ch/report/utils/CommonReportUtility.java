
package com.ch.report.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import com.ch.utils.ParallelFactory;

public class CommonReportUtility extends ParallelFactory
{
    public static String getCurrentDate()
    {
        Date date = new Date();
        DateFormat df = new SimpleDateFormat("ddMMMyyhhmmssaz");
        df.setTimeZone(TimeZone.getTimeZone("America/New_York"));
        String str = df.format(date);
        return str;
    }

    public static String getDate()
    {
        Date date = new Date();
        DateFormat df = new SimpleDateFormat("ddMMMMyyyy");
        df.setTimeZone(TimeZone.getTimeZone("America/New_York"));
        String str = df.format(date);
        return str.toUpperCase();
    }

    public static String getTime()
    {
        Date date = new Date();
        DateFormat df = new SimpleDateFormat("hh:mm a z");
        df.setTimeZone(TimeZone.getTimeZone("America/New_York"));
        String str = df.format(date);
        return str.toUpperCase();
    }

}
