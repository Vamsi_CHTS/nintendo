package com.ch.nintendo.utils;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.Dimension;

import com.ch.nintendo.utils.constants.FileConstants;
import com.ch.nintendo.utils.constants.ObjectConstants;
import com.ch.utils.DriverFactory;
import com.ch.utils.PropertyUtil;
import com.ch.utils.SeleniumUtils;

public class CommonUtils
{
    private CommonUtils()
    {

    }

    public static void NintendoURL() throws IOException
    {
        SeleniumUtils.getURL(PropertyUtil.getPropertyValue(ObjectConstants.URL, FileConstants.SERVER_PROPERTY_FILE));
    }

    public static void mobileView()
    {
        String browserName = PropertyUtil.getConfigValue("BROWSER_NAME");
        switch (browserName.trim())
        {

            case "BS_Desktop":
            case "iossafari":
                System.out.println(browserName);
                break;
            case "chrome":
                DriverFactory.getDriver().manage().window().setSize(new Dimension(375, 812));
                break;
            default:
                DriverFactory.getDriver().manage().window().setSize(new Dimension(360, 640));
        }
    }

    public static void desktopView()
    {
        DriverFactory.getDriver().manage().window().maximize();
    }

    public static void tabletView()
    {
        DriverFactory.getDriver().manage().window().setSize(new Dimension(900, 668));
    }

    public static String getCurrentDate()
    {
        Date date = new Date();
        DateFormat df = new SimpleDateFormat("ddMMyyhhmmss");
        String str = df.format(date);
        return str;
    }

    public static float getFloatFromString(String value)
    {
        if (value.contains("$") && value.contains(",") && value.contains("."))
        {

            String[] a = value.split(",");
            String[] b = a[0].split("\\$");
            String var = a[1] + b[1];
            return Float.parseFloat(var);
        } else if (value.startsWith("$") && value.contains("."))
        {

            return Float.parseFloat(value.substring(1));
        } else if (value.contains("."))
        {

            return Float.parseFloat(value);
        } else if (value.startsWith("$"))
        {

            return Float.parseFloat(value.substring(1));
        } else if (!(value.contains("$") && value.contains(".")))
        {

            return Float.parseFloat(value);
        }
        return 0;
    }
}
