package com.ch.nintendo.utils.constants;

public class FileConstants
{

    private FileConstants()
    {

    }

    public static final String TEST_HOME                      = System.getProperty("test.home", "./");

    public static final String RESOURCES_HOME                 = TEST_HOME + "resources/";

    public static final String CONFIG_HOME                    = RESOURCES_HOME + "config/";

    public static final String TESTDATA_HOME                  = RESOURCES_HOME + "testdata/";

    public static final String JSON_HOME                      = RESOURCES_HOME + "jsonfiles/";

    public static final String GEO_LOC                        = JSON_HOME + "geoloc.json";

    public static final String AUTOIT_HOME                    = RESOURCES_HOME + "AutoIt/";

    public static final String TBC_CONTACTUS_AUTOIT           = AUTOIT_HOME + "TBC_Contactus.exe";

    public static final String INVALID_ABOUT_YOU_FILE         = TESTDATA_HOME + "invalidAboutYou.csv";

    public static final String INVALID_CONTACT_INFO_FILE      = TESTDATA_HOME + "invalidContactInfo.csv";

    public static final String INVALID_SIGNIN_USER_FILE       = TESTDATA_HOME + "InvalidSignInData.csv";

    public static final String LOGIN_DATA_FILE                = TESTDATA_HOME + "LoginTestData.csv";

    public static final String INVALIDLOGIN_DATAFILE          = TESTDATA_HOME + "InvalidLoginTestData.csv";

    public static final String CARD_DETAILS_FILE              = TESTDATA_HOME + "CreditCardData.csv";

    public static final String LOAD_DATA_FILE                 = TESTDATA_HOME + "loadData.csv";

    public static final String FORGOT_PWD_DATA_FILE           = TESTDATA_HOME + "ForgotPwdTestData.csv";

    public static final String USER_DATA_FILE                 = TESTDATA_HOME + "UserData.csv";

    public static final String NEWADDRESS_DATA_FILE           = TESTDATA_HOME + "AddnewAddress.csv";

    public static final String APO_ADDRESS_DATA_FILE          = TESTDATA_HOME + "APOAddress.csv";

    public static final String FPO_ADDRESS_DATA_FILE          = TESTDATA_HOME + "FPOAddress.csv";

    public static final String INTERNATIONALADDRESS_DATA_FILE = TESTDATA_HOME + "InternationalAddress.csv";

    public static final String IMAGE_DATA_FILE                = TESTDATA_HOME + "ImageText.csv";

    // public static final String REPORTS_HOME = TEST_HOME + "reports/";

    public static final String DRIVERS_HOME                   = RESOURCES_HOME + "drivers/";

    public static final String WINDOWS_HOME                   = DRIVERS_HOME + "windows/";

    public static final String LINUX_HOME                     = DRIVERS_HOME + "linux/";

    public static final String MAC_HOME                       = DRIVERS_HOME + "mac/";

    public static final String CHROME                         = DRIVERS_HOME + "chromedriver.exe";

    public static final String GECKO                          = DRIVERS_HOME + "geckodriver.exe";

    public static final String PHANTOMJS_WIN                  = DRIVERS_HOME + "phantomjs.exe";

    public static final String I_E                            = DRIVERS_HOME + "IEDriverServer.exe";

    public static final String OPERA                          = DRIVERS_HOME + "operadriver.exe";

    public static final String TEST_PROPERTY_FILE             = CONFIG_HOME + "testconfig.properties";

    public static final String TABELT_OBJ_PROPERTY            = CONFIG_HOME + "tabletObject.properties";

    public static final String STATIC_TEXT_PROPERTY           = CONFIG_HOME + "staticText.properties";

    public static final String SERVER_PROPERTY_FILE           = CONFIG_HOME + "server.properties";

    public static final String OBJECT_PROPERTY_FILE           = CONFIG_HOME + "object.properties";

    public static final String MOBILE_OBJ_PROPERTY_FILE       = CONFIG_HOME + "mobileObject.properties";

    public static final String GECKO_WINDOWS                  = WINDOWS_HOME + "geckodriver.exe";

    public static final String CHROME_LINUX                   = "CHROME_LINUX";

    public static final String CHROME_WINDOWS                 = WINDOWS_HOME + "chromedriver.exe";

    public static final String CHROME_MAC                     = MAC_HOME + "chromedriver";

    public static final String IE_WINDOWS                     = WINDOWS_HOME + "IEDriverServer.exe";

    public static final String EDGE_WINDOWS                   = WINDOWS_HOME + "MicrosoftWebDriver.exe";

    public static final String GECKO_LINUX                    = "GECKO_LINUX";

    public static final String PLATFORM_NAME                  = "platform.name";

    public static final String PLATFORM_VERSION               = "platform.version";

    public static final String DEVICE_NAME                    = "device.name";

    public static final String BROWSE_NAME                    = "browser";

    public static final String DEVICE_URL                     = "driver.url";

    public static final String MOBILE_PC                      = "MOBILE_PC";

    public static final String PLATFORMEXE                    = "platformexe.name";

    public static final String APP_NAME                       = "app.name";

    public static final String BROWSER_NAME                   = "BROWSER_NAME";

    public static final String EMAIL_SIGNUP_DATA_FILE         = TESTDATA_HOME + "SignUpData.csv";

    public static final String LOG_PATH                       = "LOG_PATH";

    public static final String EXCEL_FILENAME                 = CONFIG_HOME + "ExcelObjectProperties.xls";

    public static final String PROJECT_TITLE                  = "Project_Title";

    public static final String EMAILS                         = "EMAILS";
    public static final String EMAILS_USERNAME                = "EMAILS_USERNAME";
    public static final String EMAILS_PW                      = "EMAILS_PW";

    public static final String INVALID_CREATE_ACCOUNT_FILE    = TESTDATA_HOME + "InvalidCreateAccount.csv";
    public static final String EQUALS_CONTANT                 = " ==== ";
    public static final String FALSE_CONTANT                  = "false";
    public static final String TD_CONTANT                     = "</td>";
    public static final String TD_CONTANT1                    = "<td>";
    public static final String TRTD_CONTANT                   = "<tr><td>";
    public static final String TDTD_CONSTANT                  = "</td><td>";
    public static final String TDTR_CONSTANT                  = "</td></tr>";
    public static final String REPORTSLOGS_CONSTANT           = "/Reports/logs/";

    public static final String LOGFILECONTROL_CONSTRANT       = "LOGFILE_CONTROL";
    public static final String TARGETHREF_CONSTRANT           = "<a target=\"_balnk\" href=\"";

    public static final String RELEASE_VERSION                = "RELEASE_VERSION";

    public static final String CHROME_VERSION                 = "CHROME_VERSION";

    public static final String FIREFOX_VERSION                = "FIREFOX_VERSION";

    public static final String SAFARI_VERSION                 = "SAFARI_VERSION";

    public static final String IE_VERSION                     = "IE_VERSION";

    public static final String MONGO_DB_IP                    = "MONGO_DB_IP";

    public static final String MONGO_DB_PORT                  = "MONGO_DB_PORT";

    public static final String ERROR_CONSTRANT                = "ERROR";
    public static final String FAILED_CONSTRANT               = "FAIL";
    public static final String BROWSER_VERSION                = "browserversion";

    public static final String BROWSER_CHROME                 = "chrome";
    public static final String BROWSER_FIREFOX                = "firefox";
    public static final String BROWSER_IE                     = "ie";
    public static final String BROWSER_SAFARI                 = "safari";

    public static final String ATTACHMENT_CONSTRANT           = "attachment";
    public static final String STACKTRACE_CONSTRANT           = "stackTrace";
    public static final String SITENAME_CONSTRANT             = "siteName";
    public static final String ENVDETAILS_CONSTRANT           = "environmentDetails";
    public static final String EXECUTIONID_CONSTRANT          = "executionId";
    public static final String RELEASEVERSION_CONSTRANT       = "releaseVersion";
    public static final String RESULT_CONSTRANT               = "result";
    public static final String TCNAME_CONSTRANT               = "testCaseName";
    public static final String TCLASSNAME_CONSTRANT           = "testClassName";
    public static final String TSUITNAME_CONSTRANT            = "testSuitName";

    public static final String MONGO_DBNAME                   = "MONGO_DBNAME";

    public static final String MONGO_DESKTOP_COLLECTION       = "MONGO_DESKTOP_COLLECTION";

    public static final String MONGO_MOBILE_COLLECTION        = "MONGO_MOBILE_COLLECTION";

    public static final String MONGO_TABLET_COLLECTION        = "MONGO_TABLET_COLLECTION";

    public static final String MONGO_CRASH_COLLECTION         = "MONGO_CRASH_COLLECTION";

    public static final String AUTH_CHROME                    = AUTOIT_HOME + "AuthenticationChrome.exe";

    public static final String AUTH_FIREFOX                   = AUTOIT_HOME + "AuthenticationFirefox.exe";

    public static final String AUTH_IE                        = AUTOIT_HOME + "AuthenticationIE.exe";

    // BrowserStack Constants
    public static final String BS_USERNAME                    = "BS_USERNAME";
    public static final String BS_ACCESSKEY                   = "BS_ACCESSKEY";
    public static final String BS_BROWSER                     = "BS_BROWSER";
    public static final String BS_BROWSER_VERSION             = "BS_BROWSER_VERSION";
    public static final String BS_OS                          = "BS_OS";
    public static final String BS_OS_VERSION                  = "BS_OS_VERSION";
    public static final String BS_MB_PLATFORM                 = "BS_MB_PLATFORM";
    public static final String BS_MB_DEVICE                   = "BS_MB_DEVICE";
    public static final String BS_MB_OS_VERSION               = "BS_MB_OS_VERSION";
    public static final String BS_MB_EXECUTION_NAME           = "BS_MB_EXECUTION_NAME";
    public static final String BS_REAL_DEVICE                 = "BS_REAL_DEVICE";

}
