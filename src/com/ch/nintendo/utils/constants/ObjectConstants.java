package com.ch.nintendo.utils.constants;

public class ObjectConstants
{

    private ObjectConstants()
    {

    }

    public static final String URL               = "url";

    public static final String SEARCH_TEXT_BOX   = "SEARCH_TEXT_BOX";

    public static final String CLICK_SEARCH_ICON = "CLICK_SEARCH_ICON";

    public static final String FIRST_SKU_IMAGE   = "FIRST_SKU_IMAGE";

}
