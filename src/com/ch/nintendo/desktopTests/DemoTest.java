package com.ch.nintendo.desktopTests;

import java.io.IOException;
import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.ch.nintendo.desktopPages.DemoDesktopPage;
import com.ch.nintendo.utils.CommonUtils;
import com.ch.report.utils.AbstractTestCaseReport;
import com.ch.reports.TestCaseDetail;
import com.ch.reports.TestCaseFactory;
import com.ch.retry.Retry;
import com.ch.utils.SeleniumUtils;

public class DemoTest extends AbstractTestCaseReport
{

    public void NintendoURL() throws IOException
    {
        CommonUtils.desktopView();
        CommonUtils.NintendoURL();
    }

    @Test(groups = { "Smoke", "Search", "P1" })
    public void searchFunctionality(Method m)
    {
        String name = new Object()
        {}.getClass().getEnclosingMethod().getName();
        TestCaseDetail testcase = TestCaseFactory.createTestCaseDetail(this.getClass(), name, m);
        Retry retry = new Retry(4);
        while (retry.retry())
        {
            try
            {
                NintendoURL();
                SeleniumUtils.wait(6);
                DemoDesktopPage.enterSearchField("game");
                SeleniumUtils.wait(1);
                DemoDesktopPage.clickSearchIcon();
                SeleniumUtils.wait(4);
                testcase.assertTrue(DemoDesktopPage.isSKUResult(), "System should display the SKU results.",
                        "System is not displaying the SKU results.");
                testcase.pass("System is able to navigate to the SKU listing page and displaying the results.");
                AbstractTestCaseReport.sendPassOrFailInfo(testcase);
                break;
            } catch (Exception e)
            {
                testcase.retry("System is not able to navigate to the SKU listing page and displaying the results.",
                        testcase, retry, e);
                e.printStackTrace();
            }
        }
    }

    @Test(groups = { "Smoke", "Search", "P1" })
    public void searchFunctionality1(Method m)
    {
        String name = new Object()
        {}.getClass().getEnclosingMethod().getName();
        TestCaseDetail testcase = TestCaseFactory.createTestCaseDetail(this.getClass(), name, m);
        Retry retry = new Retry(4);
        while (retry.retry())
        {
            try
            {
                NintendoURL();
                SeleniumUtils.wait(6);
                DemoDesktopPage.enterSearchField("game");
                SeleniumUtils.wait(1);
                DemoDesktopPage.clickSearchIcon();
                SeleniumUtils.wait(4);
                testcase.assertTrue(DemoDesktopPage.isSKUResult(), "System should display the SKU results.",
                        "System is not displaying the SKU results.");
                testcase.pass("System is able to navigate to the SKU listing page and displaying the results.");
                AbstractTestCaseReport.sendPassOrFailInfo(testcase);
                break;
            } catch (Exception e)
            {
                testcase.retry("System is not able to navigate to the SKU listing page and displaying the results.",
                        testcase, retry, e);
                e.printStackTrace();
            }
        }
    }

    @Test(groups = { "Smoke", "Search", "P1" })
    public void searchFunctionality2(Method m)
    {
        String name = new Object()
        {}.getClass().getEnclosingMethod().getName();
        TestCaseDetail testcase = TestCaseFactory.createTestCaseDetail(this.getClass(), name, m);
        Retry retry = new Retry(4);
        while (retry.retry())
        {
            try
            {
                NintendoURL();
                SeleniumUtils.wait(6);
                DemoDesktopPage.enterSearchField("game");
                SeleniumUtils.wait(1);
                DemoDesktopPage.clickSearchIcon();
                SeleniumUtils.wait(4);
                testcase.assertTrue(DemoDesktopPage.isSKUResult(), "System should display the SKU results.",
                        "System is not displaying the SKU results.");
                testcase.pass("System is able to navigate to the SKU listing page and displaying the results.");
                AbstractTestCaseReport.sendPassOrFailInfo(testcase);
                break;
            } catch (Exception e)
            {
                testcase.retry("System is not able to navigate to the SKU listing page and displaying the results.",
                        testcase, retry, e);
                e.printStackTrace();
            }
        }
    }

    @Test(groups = { "Smoke", "Search", "P1" })
    public void searchFunctionality3(Method m)
    {
        String name = new Object()
        {}.getClass().getEnclosingMethod().getName();
        TestCaseDetail testcase = TestCaseFactory.createTestCaseDetail(this.getClass(), name, m);
        Retry retry = new Retry(4);
        while (retry.retry())
        {
            try
            {
                NintendoURL();
                SeleniumUtils.wait(6);
                DemoDesktopPage.enterSearchField("game");
                SeleniumUtils.wait(1);
                DemoDesktopPage.clickSearchIcon();
                SeleniumUtils.wait(4);
                testcase.assertTrue(DemoDesktopPage.isSKUResult(), "System should display the SKU results.",
                        "System is not displaying the SKU results.");
                testcase.pass("System is able to navigate to the SKU listing page and displaying the results.");
                AbstractTestCaseReport.sendPassOrFailInfo(testcase);
                break;
            } catch (Exception e)
            {
                testcase.retry("System is not able to navigate to the SKU listing page and displaying the results.",
                        testcase, retry, e);
                e.printStackTrace();
            }
        }
    }

    @Test(groups = { "Smoke", "Search", "P1" })
    public void searchFunctionality4(Method m)
    {
        String name = new Object()
        {}.getClass().getEnclosingMethod().getName();
        TestCaseDetail testcase = TestCaseFactory.createTestCaseDetail(this.getClass(), name, m);
        Retry retry = new Retry(4);
        while (retry.retry())
        {
            try
            {
                NintendoURL();
                SeleniumUtils.wait(6);
                DemoDesktopPage.enterSearchField("game");
                SeleniumUtils.wait(1);
                DemoDesktopPage.clickSearchIcon();
                SeleniumUtils.wait(4);
                testcase.assertTrue(DemoDesktopPage.isSKUResult(), "System should display the SKU results.",
                        "System is not displaying the SKU results.");
                testcase.pass("System is able to navigate to the SKU listing page and displaying the results.");
                AbstractTestCaseReport.sendPassOrFailInfo(testcase);
                break;
            } catch (Exception e)
            {
                testcase.retry("System is not able to navigate to the SKU listing page and displaying the results.",
                        testcase, retry, e);
                e.printStackTrace();
            }
        }
    }

    @Test(groups = { "Smoke", "Search", "P1" })
    public void searchFunctionality5(Method m)
    {
        String name = new Object()
        {}.getClass().getEnclosingMethod().getName();
        TestCaseDetail testcase = TestCaseFactory.createTestCaseDetail(this.getClass(), name, m);
        Retry retry = new Retry(4);
        while (retry.retry())
        {
            try
            {
                NintendoURL();
                SeleniumUtils.wait(6);
                DemoDesktopPage.enterSearchField("game");
                SeleniumUtils.wait(1);
                DemoDesktopPage.clickSearchIcon();
                SeleniumUtils.wait(4);
                testcase.assertTrue(DemoDesktopPage.isSKUResult(), "System should display the SKU results.",
                        "System is not displaying the SKU results.");
                testcase.pass("System is able to navigate to the SKU listing page and displaying the results.");
                AbstractTestCaseReport.sendPassOrFailInfo(testcase);
                break;
            } catch (Exception e)
            {
                testcase.retry("System is not able to navigate to the SKU listing page and displaying the results.",
                        testcase, retry, e);
                e.printStackTrace();
            }
        }
    }
}
