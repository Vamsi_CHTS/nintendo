package com.ch.nintendo.desktopPages;

import com.ch.ExcelUtils.ExcelBean;
import com.ch.ExcelUtils.ExcelProperty;
import com.ch.nintendo.utils.constants.ModuleNames;
import com.ch.nintendo.utils.constants.ObjectConstants;
import com.ch.utils.SeleniumUtils;

public class DemoDesktopPage
{

    public static void enterSearchField(String searchText)
    {
        SeleniumUtils.sendKeys(ExcelProperty.getElementValue(ModuleNames.DEMOTEST, ObjectConstants.SEARCH_TEXT_BOX),
                searchText);

    }

    public static void clickSearchIcon()
    {
        SeleniumUtils.click(ExcelProperty.getElementValue(ModuleNames.DEMOTEST, ObjectConstants.CLICK_SEARCH_ICON));
    }

    public static boolean isSKUResult()
    {
        ExcelBean element1 = ExcelProperty.getElementValue(ModuleNames.DEMOTEST, ObjectConstants.FIRST_SKU_IMAGE);

        boolean flag = false;
        try
        {

            flag = SeleniumUtils.iSDisplayed(element1);

        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return flag;
    }

}
