package com.ch.ExcelUtils;

import java.util.HashMap;
import java.util.List;

import com.ch.reports.MyException;

public class ExcelProperty extends ReadExcelUtils {
	
	
	
	static HashMap<String, ExcelSheet> excelSheet = null;

	MyException ex = new MyException("");
	
/*	public ExcelProperty() {
	}*/

	public static ExcelBean getElementValue(String sheetName, String key)  {
		if (excelSheet == null) {
			excelSheet = new HashMap();
			List<ExcelSheet> sheets = readBooksFromExcelFile();
			for (ExcelSheet excelSheet : sheets) {
//				System.out.println(excelSheet);
			}
		}
		ExcelSheet sheet = (ExcelSheet) excelSheet.get(sheetName);
		ExcelBean bean = null ;
		if (sheet == null) {
//			throw new Exception("Sheet name is wrong");
			try {
				throw new MyException("Sheet name is wrong");
			} catch (MyException e) {

				e.printStackTrace();
			}
			
		}
		else {
		bean = sheet.getBean(key);
		if (bean == null) {
//			throw new Exception("Key name is wrong");
			try {
				throw new MyException("Sheet name is wrong");
			} catch (MyException e) {

				e.printStackTrace();
			}
		}
	}
		
		return bean;
	}

	public HashMap<String, ExcelSheet> getExcelSheet() {
		return excelSheet;
	}

	public static void setExcelSheet(String name, ExcelSheet sheet) {
		excelSheet.put(name, sheet);
	}
}
