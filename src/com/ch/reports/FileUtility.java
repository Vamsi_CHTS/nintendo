
package com.ch.reports;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public class FileUtility {
	
	
	private FileUtility()
	{
		
	}
	public static String readContent(String file) {
		StringBuilder content = new StringBuilder();

		String substring = file.substring(1);
		InputStream resourceAsStream = FileUtility.class.getResourceAsStream(substring);

		if (resourceAsStream == null) {
			try (BufferedReader reader = new BufferedReader(
					new InputStreamReader(new FileInputStream(new File(file))))) {
				// resourceAsStream = new FileInputStream(new File(file));

				// BufferedReader reader = new BufferedReader(new InputStreamReader(new
				// FileInputStream(new File(file))));
				String line = reader.readLine();
				while (null != line) {
					content.append(line + "\n");
					line = reader.readLine();
				}
				reader.close();
			} catch (IOException e) {

				e.printStackTrace();
			}
			
		}
		return content.toString();
	}

    public static void writeHTMLContent(String file, String content)
    {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) 
        {
			
			writer.write(content);
			writer.close();
		} catch (IOException e) {

			e.printStackTrace();
		}
    }

}
