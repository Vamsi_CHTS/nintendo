package com.ch.reports;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.bson.Document;
import org.json.simple.JSONObject;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.ch.nintendo.utils.constants.FileConstants;
import com.ch.report.utils.AbstractTestCaseReport;
import com.ch.report.utils.CommonReportUtility;
import com.ch.report.utils.ReportFileConstants;
import com.ch.retry.Retry;
import com.ch.utils.DriverFactory;
import com.ch.utils.PropertyUtil;

public class TestCaseDetail
{
    String                      smokeName        = "";
    String                      moduleName       = "";
    String                      priority         = "";
    String                      testsuitename    = "";
    String                      testcasename     = "";
    String                      expectedresult   = "";
    String                      result           = "";
    String                      errorlog         = "";
    String                      exceptionmessage = "";
    String                      comment          = "";
    String                      exactresult      = "";
    byte[]                      imagedata        = null;
    String                      imagestring      = "";
    String                      trace            = "";
    String                      testcasedate     = "";
    String                      testcasetime     = "";
    private final static Logger LOGGER           = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public TestCaseDetail(String testsuitename, String tcname, String expectdresult, String result, String errorlog,
            String excepmsg)
    {
        this.testsuitename = testsuitename;
        testcasename = tcname;
        this.expectedresult = expectdresult;
        this.result = result;
        this.errorlog = errorlog;
        exceptionmessage = excepmsg;
    }

    public TestCaseDetail(Class<?> object, String name, Method m)
    {
        this.testsuitename = object.getSimpleName();
        this.testcasename = name;
        Test testClass = m.getAnnotation(Test.class);
        ArrayList<String> priorityList = new ArrayList<String>();
        for (int i = 0; i < testClass.groups().length; i++)
        {
            if (testClass.groups()[i].equalsIgnoreCase("Smoke"))
            {
                this.smokeName = testClass.groups()[i];
            } else if (testClass.groups()[i].equalsIgnoreCase("P1") || testClass.groups()[i].equalsIgnoreCase("P2")
                    || testClass.groups()[i].equalsIgnoreCase("P3"))
            {
                priorityList.add(testClass.groups()[i]);
            } else
            {
                this.moduleName = testClass.groups()[i];
            }
        }
        this.priority = priorityList.toString().replaceAll("\\[", "").replaceAll("\\]", "").trim();
    }

    public void pass(String expectedResult)
    {

        if ("".equalsIgnoreCase(result))
        {
            this.expectedresult = expectedResult;
            result = "PASS";
            exactresult = "PASS";
            testcasedate = CommonReportUtility.getDate();
            testcasetime = CommonReportUtility.getTime();
        }
    }

    public static String getCurrentDate()
    {
        Date date = new Date();
        DateFormat df = new SimpleDateFormat("ddMMyyhhmmss");
        String str = df.format(date);
        return str;
    }

    public void setErrorLogFile(Throwable trace) throws IOException
    {
        String s = "";
        try
        {
            String fileName = "err_" + getCurrentDate();
            if (PropertyUtil.getConfigValue(FileConstants.LOGFILECONTROL_CONSTRANT).equalsIgnoreCase("Server"))
            {
                String errorFile = PropertyUtil.getConfigValue("LOG_PATH") + fileName + ".log";
                File textr = (File) ((TakesScreenshot) DriverFactory.getDriver()).getScreenshotAs(OutputType.FILE);
                String file = getCurrentDate() + ".png";
                FileUtils.copyFile(textr, new File(PropertyUtil.getConfigValue("LOG_PATH") + file));
                PrintWriter printWriter = new PrintWriter(errorFile);
                trace.printStackTrace(printWriter);
                printWriter.close();

                s = FileConstants.TARGETHREF_CONSTRANT + getMyIPAddress() + FileConstants.REPORTSLOGS_CONSTANT
                        + fileName + ".log" + "\"> <font color=\"red\">Error Log</a>" + "<br>"
                        + FileConstants.TARGETHREF_CONSTRANT + getMyIPAddress() + "/Reports/logs/" + file
                        + "\"> <font color=\"red\">Screen Shot</a>";
                this.errorlog = s;
                this.trace = trace.toString();
                BufferedImage buffImage = ImageIO.read(textr);
                ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
                ImageIO.write(buffImage, "png", bytestream);
                this.imagedata = bytestream.toByteArray();
                this.imagestring = Base64.encodeBase64String(bytestream.toByteArray());
                // new String(Base64.encodeBase64(bytestream.toByteArray()),
                // "UTF-8");
            } else if (PropertyUtil.getConfigValue(FileConstants.LOGFILECONTROL_CONSTRANT).equalsIgnoreCase("Local"))
            {
                String errorFile = fileName + ".log";
                File textr = (File) ((TakesScreenshot) DriverFactory.getDriver()).getScreenshotAs(OutputType.FILE);
                String file = getCurrentDate() + ".png";
                FileUtils.copyFile(textr, new File(ReportFileConstants.REPORTS_HOME + file));
                PrintWriter printWriter = new PrintWriter(ReportFileConstants.REPORTS_HOME + errorFile);
                trace.printStackTrace(printWriter);
                printWriter.close();
                s = "<a target=\"_balnk\" href=\"./" + errorFile + "\"> <font color=\"red\">Error Log</a>" + "<br>"
                        + FileConstants.TARGETHREF_CONSTRANT + "./" + file + "\"> <font color=\"red\">Screen Shot</a>";
                this.errorlog = s;
                this.trace = trace.toString();
                BufferedImage buffImage = ImageIO.read(textr);
                ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
                ImageIO.write(buffImage, "png", bytestream);
                this.imagedata = bytestream.toByteArray();
                this.imagestring = Base64.encodeBase64String(bytestream.toByteArray());
            }
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    public void setScreenShot() throws IOException
    {
        File textr = ((TakesScreenshot) DriverFactory.getDriver()).getScreenshotAs(OutputType.FILE);
        String file = getCurrentDate() + ".png";
        if (PropertyUtil.getConfigValue(FileConstants.LOGFILECONTROL_CONSTRANT).equalsIgnoreCase("Server"))
        {
            FileUtils.copyFile(textr, new File(PropertyUtil.getConfigValue(FileConstants.LOG_PATH) + file));
            this.result = FileConstants.TARGETHREF_CONSTRANT + getMyIPAddress() + FileConstants.REPORTSLOGS_CONSTANT
                    + file + "\"> <font color=\"red\">FAIL</a>";
            this.exactresult = "FAIL";
            BufferedImage buffImage = ImageIO.read(textr);
            ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
            ImageIO.write(buffImage, "png", bytestream);
            this.imagedata = bytestream.toByteArray();
            this.imagestring = Base64.encodeBase64String(bytestream.toByteArray());
        } else if (PropertyUtil.getConfigValue(FileConstants.LOGFILECONTROL_CONSTRANT).equalsIgnoreCase("Local"))
        {
            FileUtils.copyFile(textr, new File(ReportFileConstants.REPORTS_HOME + file));
            // this.result = "<a target=\"_balnk\" href=\"" + "./" + file + "\">
            // <font color=\"red\">FAIL</a>";

            if (this.getResult().equalsIgnoreCase(FileConstants.ERROR_CONSTRANT))
            {
                this.result = FileConstants.TARGETHREF_CONSTRANT + "./" + file + "\"> <font color=\"red\">ERROR</a>";
                this.exactresult = FileConstants.ERROR_CONSTRANT;
                BufferedImage buffImage = ImageIO.read(textr);
                ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
                ImageIO.write(buffImage, "png", bytestream);
                this.imagedata = bytestream.toByteArray();
                this.imagestring = Base64.encodeBase64String(bytestream.toByteArray());
                // this.imagestring =
                // Base64.encodeBase64URLSafeString(this.imagedata);
            } else
            {
                this.result = FileConstants.TARGETHREF_CONSTRANT + "./" + file + "\"> <font color=\"Blue\">FAIL</a>";
                this.exactresult = "FAIL";
                BufferedImage buffImage = ImageIO.read(textr);
                ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
                ImageIO.write(buffImage, "png", bytestream);
                this.imagedata = bytestream.toByteArray();
                this.imagestring = Base64.encodeBase64String(bytestream.toByteArray());
            }
        }
    }

    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public void comments(String def)
    {
        // if (result == "PASS")
        if (result.equalsIgnoreCase("PASS"))
        {
            LOGGER.info("Script PASS");
        } else
        {
            this.comment = def;
        }
    }

    public void assertTrue(boolean flag, String expected, String actual)
    {
        if (flag == false)
        {
            fail(expected, actual);
        }
    }

    public void assertFalse(boolean flag, String expected, String actual)
    {
        if (flag == true)
        {
            fail(expected, actual);
        }
    }

    public void forceAssert(boolean flag, String expected, String actual)
    {
        if (flag == false)
        {
            fail(expected, actual);
            Assert.fail(expected);
        }
    }

    public static String getMyIPAddress()
    {
        InetAddress addr;
        String host = "";
        try
        {
            addr = InetAddress.getLocalHost();
            host = "http://" + addr.getHostAddress() + ":9080";
        } catch (UnknownHostException e)
        {
            e.printStackTrace();
        }
        return host;
    }

    public void assertEquals(String expected, String actual)
    {
        if (null == actual || null == expected || actual.equalsIgnoreCase(expected) == false)
        {
            fail(expected, actual);
        }
    }

    public void forceAssertEquals(String expected, String actual)
    {
        if (null == actual || null == expected || actual.equalsIgnoreCase(expected) == false)
        {
            fail(expected, actual);
            Assert.assertEquals(actual, expected);
        }
    }

    public void fail(String expectedResult, String actual)
    {
        if ("".equalsIgnoreCase(result))
        {
            this.expectedresult = expectedResult;
            result = "FAIL";
            exactresult = "FAIL";
            testcasedate = CommonReportUtility.getDate();
            testcasetime = CommonReportUtility.getTime();
            errorlog = actual;
            try
            {
                setScreenShot();
            } catch (IOException e)
            {

                e.printStackTrace();
            }
        } else
        {
            this.expectedresult = this.expectedresult + "\n" + expectedResult;
            result = "FAIL";
            exactresult = "FAIL";
            testcasedate = CommonReportUtility.getDate();
            testcasetime = CommonReportUtility.getTime();
            errorlog = this.errorlog + "\n" + actual;
            try
            {
                setScreenShot();
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    public void error(String expectedResult, Throwable trace)
    {
        this.expectedresult = expectedResult;
        result = FileConstants.ERROR_CONSTRANT;
        exactresult = FileConstants.ERROR_CONSTRANT;
        testcasedate = CommonReportUtility.getDate();
        testcasetime = CommonReportUtility.getTime();
        exceptionmessage = trace.getMessage();
        try
        {
            setErrorLogFile(trace);
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public String getSmokeName()
    {
        return smokeName;
    }

    public void setSmokeName(String smokename)
    {
        this.smokeName = smokename;
    }

    public String getModuleName()
    {
        return moduleName;
    }

    public void setModuleName(String modulename)
    {
        this.moduleName = modulename;
    }

    public String getPriority()
    {
        return priority;
    }

    public void setPriority(String testpriority)
    {
        this.priority = testpriority;
    }

    public String getTestsuitename()
    {
        return testsuitename;
    }

    /**
     * @param testsuitename
     *            the testsuitename to set
     */
    public void setTestsuitename(String testsuitename)
    {
        this.testsuitename = testsuitename;
    }

    /**
     * @return the testcasename
     */
    public String getTestcasename()
    {
        return testcasename;
    }

    /**
     * @param testcasename
     *            the testcasename to set
     */
    public void setTestcasename(String testcasename)
    {
        this.testcasename = testcasename;
    }

    /**
     * @return the expectedresult
     */
    public String getExpectedresult()
    {
        return expectedresult;
    }

    /**
     * @param expectedresult
     *            the expectedresult to set
     */
    public void setExpectedresult(String expectedresult)
    {
        this.expectedresult = expectedresult;
    }

    /**
     * @return the result
     */
    public String getResult()
    {
        return result;
    }

    /**
     * @param result
     *            the result to set
     */
    public void setResult(String result)
    {
        this.result = result;
    }

    /**
     * @return the result
     */
    public String getExactResult()
    {
        return exactresult;
    }

    /**
     * @param result
     *            the result to set
     */
    public void setExactResult(String exactresult)
    {
        this.exactresult = exactresult;
    }

    public String getTestcaseDate()
    {
        return testcasedate;
    }

    /**
     * @param result
     *            the result to set
     */
    public void setTestcaseDate(String testcasedate)
    {
        this.testcasedate = testcasedate;
    }

    public String getTestcaseTime()
    {
        return testcasetime;
    }

    /**
     * @param result
     *            the result to set
     */
    public void setTestcaseTime(String testcasetime)
    {
        this.testcasetime = testcasetime;
    }

    /**
     * @return the errorlog
     */
    public String getErrorlog()
    {
        return errorlog;
    }

    /**
     * @param errorlog
     *            the errorlog to set
     */
    public void setErrorlog(String errorlog)
    {
        this.errorlog = errorlog;
    }

    public void retry(String s, TestCaseDetail testcase, Retry retry, Exception e)
    {
        try
        {
            retry.startRetry();
        } catch (Exception e2)
        {
            e2.printStackTrace();
            testcase.error(s, e);

            JSONObject TestCaseDetails = new JSONObject();
            TestCaseDetails.put("smoke", testcase.getSmokeName());
            TestCaseDetails.put("priority", testcase.getPriority());
            TestCaseDetails.put("moduleName", testcase.getModuleName());
            TestCaseDetails.put("siteName", PropertyUtil.getServerValue("siteName"));
            TestCaseDetails.put("testSuitName", AbstractTestCaseReport.TestSuiteName);
            TestCaseDetails.put("testCaseName", testcase.getTestcasename());
            TestCaseDetails.put("releaseVersion", PropertyUtil.getConfigValue(FileConstants.RELEASE_VERSION));
            TestCaseDetails.put("result", testcase.getExactResult());
            TestCaseDetails.put("date", AbstractTestCaseReport.date);
            TestCaseDetails.put("time", AbstractTestCaseReport.time);
            TestCaseDetails.put("attachment", testcase.getImageString());
            TestCaseDetails.put("stackTrace", e.toString());
            // TestCaseDetails.put("suitid", 3);
            TestCaseDetails.put("executionId", AbstractTestCaseReport.executionId);
            TestCaseDetails.put("testClassName", testcase.getTestsuitename());
            TestCaseDetails.put("browserName", PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME));
            if (PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME).equalsIgnoreCase("chrome"))
            {
                TestCaseDetails.put(FileConstants.BROWSER_VERSION,
                        PropertyUtil.getConfigValue(FileConstants.CHROME_VERSION));
            } else if (PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME).equalsIgnoreCase("firefox"))
            {
                TestCaseDetails.put(FileConstants.BROWSER_VERSION,
                        PropertyUtil.getConfigValue(FileConstants.FIREFOX_VERSION));
            } else if (PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME).equalsIgnoreCase("safari"))
            {
                TestCaseDetails.put(FileConstants.BROWSER_VERSION,
                        PropertyUtil.getConfigValue(FileConstants.SAFARI_VERSION));
            } else if (PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME).equalsIgnoreCase("ie"))
            {
                TestCaseDetails.put(FileConstants.BROWSER_VERSION,
                        PropertyUtil.getConfigValue(FileConstants.IE_VERSION));
            }
            TestCaseDetails.put("environment", AbstractTestCaseReport.environmentName);
            TestCaseDetails.put("os", AbstractTestCaseReport.OS);

            Document doc = Document.parse(TestCaseDetails.toJSONString());
            if (testcase.getExactResult().trim().length() > 1)
            {
                // if (testcase.getTestsuitename().contains("Mobile")
                // ||
                // PropertyUtil.getConfigValue("MOBILE_PC").equalsIgnoreCase("mobile")
                // ||
                // PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME).contains("Mobile"))
                // {
                // AbstractTestCaseReport.mongoClient
                // .getDatabase(PropertyUtil.getConfigValue(FileConstants.MONGO_DBNAME))
                // .getCollection(PropertyUtil.getConfigValue(FileConstants.MONGO_MOBILE_COLLECTION))
                // .insertOne(doc);
                // } else if (testcase.getTestsuitename().contains("Tablet")
                // ||
                // PropertyUtil.getConfigValue("MOBILE_PC").equalsIgnoreCase("Tablet")
                // ||
                // PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME).contains("Tablet"))
                // {
                // AbstractTestCaseReport.mongoClient
                // .getDatabase(PropertyUtil.getConfigValue(FileConstants.MONGO_DBNAME))
                // .getCollection(PropertyUtil.getConfigValue(FileConstants.MONGO_TABLET_COLLECTION))
                // .insertOne(doc);
                // } else
                // {
                AbstractTestCaseReport.mongoClient.getDatabase(PropertyUtil.getConfigValue(FileConstants.MONGO_DBNAME))
                        .getCollection(PropertyUtil.getConfigValue(FileConstants.MONGO_DESKTOP_COLLECTION))
                        .insertOne(doc);
                // }
            }
        }
    }

    /**
     * @return the exceptionmessage
     */
    public String getExceptionmessage()
    {
        return exceptionmessage;
    }

    /**
     * @param exceptionmessage
     *            the exceptionmessage to set
     */
    public void setExceptionmessage(String exceptionmessage)
    {
        this.exceptionmessage = exceptionmessage;
    }

    /**
     * @return the result
     */
    public byte[] getImageData()
    {
        return imagedata;
    }

    /**
     * @param result
     *            the result to set
     */
    public void setImageData(byte[] imgdata)
    {
        this.imagedata = imgdata;
    }

    /**
     * @return the result
     */
    public String getImageString()
    {
        return imagestring;
    }

    /**
     * @param result
     *            the result to set
     */
    public void setImageString(String imageStr)
    {
        this.imagestring = imageStr;
    }

    /**
     * @return the result
     */
    public String getTrace()
    {
        return trace;
    }

    /**
     * @param result
     *            the result to set
     */
    public void setTrace(String trce)
    {
        this.trace = trce;
    }

    public void Error(String expectedResult, Throwable trace)
    {
        this.expectedresult = expectedResult;
        result = "ERROR";
        exactresult = "ERROR";
        exceptionmessage = trace.getMessage();
        try
        {
            setErrorLogFile(trace);
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void Failed(String expectedResult, Throwable trace)
    {
        this.expectedresult = expectedResult;
        result = FileConstants.FAILED_CONSTRANT;
        exactresult = FileConstants.FAILED_CONSTRANT;
        exceptionmessage = trace.getMessage();
        try
        {
            setErrorLogFile(trace);
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void fail(String expectedResult)
    {
        if ("".equalsIgnoreCase(result))
        {
            this.expectedresult = expectedResult;
            result = "FAIL";
            exactresult = "FAIL";
        }
    }

    // public void error(String expectedResult)
    // {
    // System.out.println("error :" + result);
    // System.out.println("error :" + expectedResult);
    // if ("".equalsIgnoreCase(result))
    // {
    // this.expectedresult = expectedResult;
    // result = "ERROR";
    // exactresult = "ERROR";
    // }
    // }

    // public static byte[] blobImage() throws IOException
    // {
    // TakesScreenshot scrShot = ((TakesScreenshot) DriverFactory.getDriver());
    // File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
    // String dest = System.getProperty("user.dir") + "\\imageforDB\\image" +
    // random() + ".png";
    // FileUtils.copyFile(SrcFile, new File(dest));
    // BufferedImage bImage = ImageIO.read(new File(dest));
    // ByteArrayOutputStream bos = new ByteArrayOutputStream();
    // ImageIO.write(bImage, "png", bos);
    // byte[] data = bos.toByteArray();
    // return data;
    //
    // }

    public static String random()
    {
        Date date = new Date();
        DateFormat df = new SimpleDateFormat("ddMMMyyhhmmssazdSsSss");
        String str = df.format(date);
        return str;
    }
}
