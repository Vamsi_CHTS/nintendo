
package com.ch.reports;

import java.lang.reflect.Method;
import java.util.ArrayList;

public class TestCaseFactory
{
    private TestCaseFactory()
    {}

    public static ArrayList<TestCaseDetail> testcases = new ArrayList<TestCaseDetail>();

    public static ArrayList<TestCaseDetail> getTestcases()
    {
        return testcases;
    }

    /**
     * @param testcases
     *            the testcases to set
     */
    public static void setTestcases(ArrayList<TestCaseDetail> testcases)
    {
        TestCaseFactory.testcases = testcases;
    }

    public static TestCaseDetail createTestCaseDetail(Class<?> object, String name, Method m)
    {
        TestCaseDetail testcase = new TestCaseDetail(object, name, m);
        testcases.add(testcase);

        return testcase;
    }
}