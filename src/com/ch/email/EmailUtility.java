package com.ch.email;

import java.util.Properties;
import java.util.logging.Logger;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.json.simple.JSONObject;

import com.ch.nintendo.utils.constants.FileConstants;
import com.ch.utils.PropertyUtil;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class EmailUtility
{

    private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private EmailUtility()
    {

    }

    static String username = PropertyUtil.getConfigValue(FileConstants.EMAILS_USERNAME);// "Chautomailer@chtsinc.com";
    static String password = PropertyUtil.getConfigValue(FileConstants.EMAILS_PW);      // "Test@12345$";

    public static void sendEmail(String subject, String body)
    {
        // final String username = "Chautomailer@chtsinc.com";
        // final String password = "Test@12345$";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new javax.mail.Authenticator()
        {
            @Override
            protected PasswordAuthentication getPasswordAuthentication()
            {
                return new PasswordAuthentication(username, password);
            }
        });

        try
        {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("AutomationReports@chtsinc.com"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress
                    .parse("bollinenivk@chtsinc.com" + "," + PropertyUtil.getConfigValue(FileConstants.EMAILS)));
            message.setSubject(subject);
            message.setText(body);
            message.setContent(body, "text/html;charset=gb2312");

            Transport.send(message);

            // System.out.println("Sent Sucessfully");
            LOGGER.info("Sent Sucessfully");

        } catch (MessagingException e)
        {
            // throw new RuntimeException(e);
            e.printStackTrace();
        }
    }

    public static void sendEmailStatus()
    {
        RestAssured.baseURI = "http://localhost:5000/queue/api";
        RequestSpecification request = RestAssured.given().contentType("application/json");

        JSONObject requestParams = new JSONObject();
        requestParams.put("emailsent", 1);
        requestParams.put("executioncomplete", 1);

        request.body(requestParams.toJSONString());
        Response response = request.post("/status");

        int statusCode = response.getStatusCode();
        LOGGER.info("The status code recieved: " + statusCode);
        LOGGER.info(response.body().asString());
    }

    public static void sendUnSuccessfulEmailStatus()
    {
        RestAssured.baseURI = "http://localhost:5000/queue/api";
        RequestSpecification request = RestAssured.given().contentType("application/json");

        JSONObject requestParams = new JSONObject();
        requestParams.put("emailsent", 2);
        requestParams.put("executioncomplete", 1);

        request.body(requestParams.toJSONString());
        Response response = request.post("/status");

        int statusCode = response.getStatusCode();
        LOGGER.info("The status code recieved: " + statusCode);
        LOGGER.info(response.body().asString());
    }

    public static void sendFile(String subject, String body, String folder, String fileName)
    {
        // String from = "Chautomailer@chtsinc.com";
        // final String username = "Chautomailer@chtsinc.com";
        // final String password = "Test@12345$";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new javax.mail.Authenticator()
        {
            @Override
            protected PasswordAuthentication getPasswordAuthentication()
            {
                return new PasswordAuthentication(username, password);
            }
        });

        try
        {
            Message message = new MimeMessage(session);

            message.setFrom(new InternetAddress(username));

            message.setRecipients(Message.RecipientType.TO, InternetAddress
                    .parse("bollinenivk@chtsinc.com" + "," + PropertyUtil.getConfigValue(FileConstants.EMAILS)));

            message.setSubject(subject);

            BodyPart messageBodyPart = new MimeBodyPart();

            messageBodyPart.setText(body);
            messageBodyPart.setContent(body, "text/html;charset=gb2312");

            Multipart multipart = new MimeMultipart();

            multipart.addBodyPart(messageBodyPart);

            messageBodyPart = new MimeBodyPart();
            String filename = folder;
            DataSource source = new FileDataSource(filename);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(fileName);
            multipart.addBodyPart(messageBodyPart);

            message.setContent(multipart);

            Transport.send(message);

            // System.out.println("Sent message successfully....");
            LOGGER.info("Sent message successfully....");
            sendEmailStatus();
            LOGGER.info("Sent email status code successfully....");
        } catch (MessagingException e)
        {
            sendUnSuccessfulEmailStatus();
            // throw new RuntimeException(e);
            e.printStackTrace();
        }
    }
}
