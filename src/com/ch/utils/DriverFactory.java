package com.ch.utils;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import com.ch.nintendo.utils.constants.FileConstants;

public class DriverFactory extends PropertyUtil
{

    private static ThreadLocal<WebDriver> webDriver = new ThreadLocal<>();

    public static WebDriver getDriver()
    {
        return webDriver.get();
    }

    public static void setWebDriver(WebDriver driver)
    {
        webDriver.set(driver);
    }

    public static WebDriver webInstance(String browserName) throws MalformedURLException
    {
        WebDriver driver = null;
        String USERNAME = PropertyUtil.getConfigValue("BS_USERNAME");
        String AUTOMATE_KEY = PropertyUtil.getConfigValue("BS_ACCESSKEY");;
        String BSURL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
        DesiredCapabilities capabilities = new DesiredCapabilities();
        if (browserName.equalsIgnoreCase("firefox"))
        {
            if (PropertyUtil.getConfigValue("GRID").equalsIgnoreCase("true"))
            {
                capabilities = DesiredCapabilities.firefox();
                capabilities.setCapability("marionette", true);
                FirefoxProfile profile = new FirefoxProfile();
                profile.setPreference("dom.popup_maximum", 0);
                capabilities.setCapability(FirefoxDriver.PROFILE, profile);
            } else if (PropertyUtil.getConfigValue("GRID").equalsIgnoreCase(FileConstants.FALSE_CONTANT))
            {
                System.setProperty("webdriver.gecko.driver", FileConstants.GECKO_WINDOWS);
                FirefoxProfile geoDisabled = new FirefoxProfile();
                geoDisabled.setPreference("geo.enabled", true);
                geoDisabled.setPreference("geo.provider.use_corelocation", true);
                geoDisabled.setPreference("geo.prompt.testing", true);
                geoDisabled.setPreference("geo.prompt.testing.allow", true);
                geoDisabled.setPreference("geo.wifi.uri", FileConstants.GEO_LOC);
                capabilities.setCapability("firefox_profile", (Object) geoDisabled);
                driver = new FirefoxDriver((Capabilities) capabilities);
            }
        }
        if (browserName.equalsIgnoreCase("IE"))
        {
            if (PropertyUtil.getConfigValue("GRID").equalsIgnoreCase("true"))
            {
                capabilities = DesiredCapabilities.internetExplorer();
            } else if (PropertyUtil.getConfigValue("GRID").equalsIgnoreCase(FileConstants.FALSE_CONTANT))
            {
                System.setProperty("webdriver.ie.driver", FileConstants.IE_WINDOWS);
                // InternetExplorerOptions options = new
                // InternetExplorerOptions();
                // options.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS,
                // true);
                driver = new InternetExplorerDriver();
            }
        }
        if (browserName.equalsIgnoreCase("safari"))
        {
            if (PropertyUtil.getConfigValue("GRID").equalsIgnoreCase("true"))
            {
                capabilities = DesiredCapabilities.safari();
            } else if (PropertyUtil.getConfigValue("GRID").equalsIgnoreCase(FileConstants.FALSE_CONTANT))
            {
                driver = new SafariDriver();
            }
        }
        if (browserName.equalsIgnoreCase("Edge"))
        {
            if (PropertyUtil.getConfigValue("GRID").equalsIgnoreCase("true"))
            {
                capabilities = DesiredCapabilities.edge();
            } else if (PropertyUtil.getConfigValue("GRID").equalsIgnoreCase(FileConstants.FALSE_CONTANT))
            {
                // System.setProperty("webdriver.edge.driver",
                // FileConstants.EDGE_WINDOWS);
                driver = new EdgeDriver();
            }
        }
        if (browserName.equalsIgnoreCase("opera"))
        {
            if (PropertyUtil.getConfigValue("GRID").equalsIgnoreCase("true"))
            {
                capabilities = DesiredCapabilities.operaBlink();
            } else if (PropertyUtil.getConfigValue("GRID").equalsIgnoreCase(FileConstants.FALSE_CONTANT))
            {
                System.setProperty("webdriver.opera.driver", FileConstants.OPERA);
                driver = new OperaDriver();
            }
        }
        if (browserName.equalsIgnoreCase("mac_chrome"))
        {
            if (PropertyUtil.getConfigValue("GRID").equalsIgnoreCase("true"))
            {
                capabilities = DesiredCapabilities.chrome();
            } else if (PropertyUtil.getConfigValue("GRID").equalsIgnoreCase(FileConstants.FALSE_CONTANT))
            {
                System.setProperty("webdriver.chrome.driver", FileConstants.CHROME_MAC);
                driver = new ChromeDriver();
            }
        }

        if (browserName.equalsIgnoreCase("chrome"))
        {
            if (PropertyUtil.getConfigValue("GRID").equalsIgnoreCase("true"))
            {
                capabilities = DesiredCapabilities.chrome();
            } else if (PropertyUtil.getConfigValue("GRID").equalsIgnoreCase(FileConstants.FALSE_CONTANT))
            {
                System.setProperty("webdriver.chrome.driver", FileConstants.CHROME_WINDOWS);
                driver = new ChromeDriver();
            }
        }

        if (browserName.equalsIgnoreCase("phantomjsWin"))
        {
            if (PropertyUtil.getConfigValue("GRID").equalsIgnoreCase("true"))
            {
                capabilities = DesiredCapabilities.phantomjs();
            } else if (PropertyUtil.getConfigValue("GRID").equalsIgnoreCase(FileConstants.FALSE_CONTANT))
            {
                System.setProperty("phantomjs.binary.path", FileConstants.PHANTOMJS_WIN);
                driver = new PhantomJSDriver();
            }
        }
        if (PropertyUtil.getConfigValue("GRID").equalsIgnoreCase("true"))
        {
            driver = new RemoteWebDriver(new URL(PropertyUtil.getConfigValue("GRID_HUB_URL")),
                    (Capabilities) capabilities);
        }
        // browserName.equalsIgnoreCase("mobileEmulation");

        // ***************BS************START************

        if (browserName.equalsIgnoreCase("BS_Desktop"))
        {

            capabilities.setCapability("browser", PropertyUtil.getConfigValue("BS_BROWSER"));
            capabilities.setCapability("browser_version", PropertyUtil.getConfigValue("BS_BROWSER_VERSION"));
            capabilities.setCapability("os", PropertyUtil.getConfigValue("BS_OS"));
            capabilities.setCapability("os_version", PropertyUtil.getConfigValue("BS_OS_VERSION"));
            capabilities.setCapability("resolution", "1024x768");

            driver = new RemoteWebDriver(new URL(BSURL), capabilities);

        }

        if (browserName.equalsIgnoreCase("iossafari"))
        {

            capabilities.setCapability("browserName", PropertyUtil.getConfigValue("BS_MB_PLATFORM"));
            capabilities.setCapability("device", PropertyUtil.getConfigValue("BS_MB_DEVICE"));
            capabilities.setCapability("realMobile", PropertyUtil.getConfigValue("BS_REAL_DEVICE"));
            capabilities.setCapability("os_version", PropertyUtil.getConfigValue("BS_MB_OS_VERSION"));
            capabilities.setCapability("name", PropertyUtil.getConfigValue("BS_MB_EXECUTION_NAME"));
            capabilities.setCapability("build", "35.0");

            driver = new RemoteWebDriver(new URL(BSURL), capabilities);

        }

        // ***************BS************END**************

        return driver;
    }

    public static WebDriver getmobileDriver()
    {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("platformName", getConfigValue(FileConstants.PLATFORM_NAME));
        caps.setCapability("app", getConfigValue(FileConstants.APP_NAME));
        caps.setCapability("platformVersion", getConfigValue(FileConstants.PLATFORM_VERSION));
        caps.setCapability("deviceName", getConfigValue(FileConstants.DEVICE_NAME));
        caps.setCapability(CapabilityType.BROWSER_NAME, getConfigValue(FileConstants.BROWSE_NAME));
        caps.setCapability(CapabilityType.PLATFORM, FileConstants.PLATFORMEXE);
        return new RemoteWebDriver(getURL(), caps);
    }

    public static URL getURL()
    {
        try
        {
            return new URL(getConfigValue(FileConstants.DEVICE_URL));
        } catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        return null;
    }
}