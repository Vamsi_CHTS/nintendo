package com.ch.utils;

import java.net.MalformedURLException;
import java.util.logging.Logger;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import com.ch.nintendo.utils.constants.FileConstants;

public class ParallelFactory
{

    private static final String CHANGE = PropertyUtil.getConfigValue(FileConstants.MOBILE_PC);

    @BeforeMethod(alwaysRun = true)
    public void beforeInvocation()
    {
        try
        {
            resetWebDriver();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public static void resetWebDriver()
    {
        quitDriver();
        try
        {
            if (CHANGE.equalsIgnoreCase("pc"))
            {
                LOGGER.info("***************************");
                WebDriver driver;

                driver = DriverFactory.webInstance(PropertyUtil.getConfigValue(FileConstants.BROWSER_NAME));
                DriverFactory.setWebDriver(driver);

            }
            if (CHANGE.equalsIgnoreCase("mobile"))
            {
                LOGGER.info("***************************");
                WebDriver driver = DriverFactory.getmobileDriver();
                DriverFactory.setWebDriver(driver);
            }
        } catch (MalformedURLException e)
        {

            e.printStackTrace();
        }
    }

    @AfterMethod(alwaysRun = true)
    public void afterInvocation()
    {
        quitDriver();
    }

    private static void quitDriver()
    {
        WebDriver driver = DriverFactory.getDriver();
        boolean b = driver != null;
        if (b)
        {
            driver.quit();
            driver = null;
        }
    }

}