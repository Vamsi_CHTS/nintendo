#!flask/bin/python
from flask import Flask, jsonify, abort, make_response, request
from flask_cors import CORS, cross_origin

app = Flask(__name__)
CORS(app, support_credentials=False)
app.config['CORS_HEADERS'] = 'Content-Type'

statuses = [
    {
        "emailsent": 0,
        "executioncomplete": 0
    }
]

path = [
    {
        "reportname": "",
		"executionId": "",
		"browserName": ""
    }
]

resultstatus = [
    {
        "passpercentage":0,
        "failpercentage":0,
        "errorpercentage":0,
        "executiontime":0,
        "testcasecount":0
    }
]

storetime = [
    {
        "starttime": "",
        "endtime": ""
    }
]

@app.route('/queue/api/status', methods=['GET'])
@cross_origin()
def get_email_status():
    return jsonify({'status': statuses[0]}), 200


@app.route('/queue/api/reportname', methods=['GET'])
@cross_origin()
def get_report_path():
    return jsonify({'status': path[0]}), 200

@app.route('/queue/api/resultstatus', methods=['GET'])
@cross_origin()
def get_result_status():
    return jsonify({'status': resultstatus[0]}), 200

@app.route('/queue/api/executiontimes', methods=['GET'])
@cross_origin()
def get_execution_times():
    return jsonify({'status': storetime[0]}), 200

@app.route('/queue/api/status', methods=['POST'])
@cross_origin()
def post_email_status():
    # if not request.json or not 'emailsent' in request.json or not 'executioncomplete' in request.json:
    #    abort(400)

    statuses[0]['emailsent'] = request.json['emailsent']
    statuses[0]['executioncomplete'] = request.json['executioncomplete']
    return jsonify({'status': statuses[0]}), 201


@app.route('/queue/api/reportname', methods=['POST'])
@cross_origin()
def post_report_path():
    path[0]['reportname']= request.json['reportname']
    path[0]['executionId'] = request.json['executionId']
    path[0]['browserName'] = request.json['browserName']
    return jsonify({'status': path[0]}), 201

@app.route('/queue/api/resultstatus', methods=['POST'])
@cross_origin()
def post_result_status():
    resultstatus[0]['passpercentage'] = request.json['passpercentage']
    resultstatus[0]['failpercentage'] = request.json['failpercentage']
    resultstatus[0]['errorpercentage'] = request.json['errorpercentage']
    resultstatus[0]['executiontime'] = request.json['executiontime']
    resultstatus[0]['testcasecount'] = request.json['testcasecount']
    return jsonify({'status': resultstatus[0]}), 201

@app.route('/queue/api/executiontimes', methods=['POST'])
@cross_origin()
def post_execution_times():
    storetime[0]['starttime'] = request.json['starttime']
    storetime[0]['endtime'] = request.json['endtime']
    return jsonify({'status': storetime[0]}), 201


if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=5000)
